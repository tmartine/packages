#!/bin/bash
set -ex
# libffi is needed for llvm-config
pacman --sync --refresh --noconfirm base-devel rsync unzip git libffi llvm \
  clang python
useradd -m -s /bin/bash ci
echo ci      ALL=\(ALL\) NOPASSWD:ALL >>/etc/sudoers
