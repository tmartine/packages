#!/bin/bash
set -ex
curl >~/opam --location \
  https://github.com/ocaml/opam/releases/download/2.0.6/opam-2.0.6-x86_64-linux
chmod +x ~/opam && sudo mv ~/opam /usr/local/bin/opam
opam init --disable-sandboxing --auto-setup --dot-profile=/home/ci/.bash_env
