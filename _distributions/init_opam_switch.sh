#!/bin/bash
set -ex
ocaml_version="$1"
base_path=packages

if [ ! -z "$ocaml_version" ]; then
  opam update && opam switch "$ocaml_version"
fi

opam install --yes dune

opam pin add --yes --no-action --kind=path "$base_path"/stdcompat

( cd "$base_path"/redirect && dune build redirect.opam && \
  opam pin add --yes --no-action --kind=path . )

( cd "$base_path"/ocamlcodoc && dune build ocamlcodoc.opam && \
  opam pin add --yes --no-action --kind=path . )

opam pin add --yes --no-action --kind=path "$base_path"/pyml

( cd "$base_path"/metapp && dune build metapp.opam && \
  opam pin add --yes --no-action --kind=path . )

( cd "$base_path"/metaquot && dune build metaquot.opam && \
  opam pin add --yes --no-action --kind=path . )

( cd "$base_path"/refl && dune build refl.opam && \
  opam pin add --yes --no-action --kind=path . )

( cd "$base_path"/pattern && dune build pattern.opam && \
  opam pin add --yes --no-action --kind=path . )

opam pin add --yes --no-action --kind=path "$base_path"/clangml

( cd "$base_path"/clangml-transforms && dune build clangml-transforms.opam && \
  opam pin add --yes --no-action --kind=path . )

( cd "$base_path"/override && dune build override.opam && \
  opam pin add --yes --no-action --kind=path . )

( cd "$base_path"/traverse && dune build traverse.opam && \
  opam pin add --yes --no-action --kind=path . )

opam update && opam install --yes depext
