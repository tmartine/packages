#!/bin/bash
set -ex
packages=`cat package_list`
sudo apt-get update
opam depext --yes --with-test $packages
