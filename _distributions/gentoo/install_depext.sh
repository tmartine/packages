#!/bin/bash
set -ex
# depext does not appear to work with gentoo
# Detecting depexts using vars: arch=x86_64, os=linux, os-distribution=gentoo, os-family=gentoo
# sys-devel/clang
# The following system packages are needed:
# Fatal error: exception Depext.Signaled_or_stopped(_, _)
# sudo emerge --sync --quiet
