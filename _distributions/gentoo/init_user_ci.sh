#!/bin/bash
set -ex
emerge --sync --quiet
# Remove warning for running in unprivileged docker
# (see https://bugs.gentoo.org/680456)
export FEATURES="-ipc-sandbox -mount-sandbox -network-sandbox -pid-sandbox \
  -sandbox -usersandbox"
emerge --quiet-build=y sudo tar dev-vcs/git net-misc/curl sys-devel/clang
# emerge sudo dev-vcs/mercurial dev-vcs/darcs
useradd -m -s /bin/bash ci
echo ci      ALL=\(ALL\) NOPASSWD:ALL >>/etc/sudoers
# Get rid of the "setrlimit(RLIMIT_CORE): Operation not permitted" warning
# See https://bugzilla.redhat.com/show_bug.cgi?id=1773148
echo Set disable_coredump false >>/etc/sudo.conf
