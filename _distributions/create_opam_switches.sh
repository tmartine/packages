#!/bin/bash
set -ex
ocaml_versions="$*"
for ocaml_version in $ocaml_versions; do
    opam switch create "$ocaml_version" "ocaml-base-compiler.$ocaml_version"
done
