#!/bin/bash
set -ex
packages="`cat package_list`"
opam switch remove --yes 4.10.0 || true
opam switch create 4.10.0
eval $(opam env)
_distributions/init_opam_switch.sh
opam depext --install --yes $packages
