#!/bin/bash
set -ex
packages=`cat package_list`
opam install --yes --with-test --verbose $packages
